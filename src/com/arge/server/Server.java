package com.arge.server;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.XmlRpcServer;
import org.apache.xmlrpc.server.XmlRpcServerConfigImpl;
import org.apache.xmlrpc.webserver.WebServer;

import java.io.IOException;

public class Server implements IServer {

  private WebServer server;
  private int port;

  PropertyHandlerMapping phm;
  XmlRpcServer xmlRpcServer;

  public Server(int port) {
    this.port = port;
    server = new WebServer(port);

    xmlRpcServer = server.getXmlRpcServer();

    phm = new PropertyHandlerMapping();
    xmlRpcServer.setHandlerMapping(phm);

    XmlRpcServerConfigImpl serverConfig = (XmlRpcServerConfigImpl) xmlRpcServer.getConfig();
    serverConfig.setEnabledForExtensions(true);
    serverConfig.setContentLengthOptional(false);
  }

  @Override
  public void start() throws IOException {
    server.start();
    System.out.println("Server started on port: " + port);
  }

  @Override
  public void addHandler(String key, Class clazz) {
    try {
      phm.addHandler(key, clazz);
      //xmlRpcServer.setHandlerMapping(phm);
    } catch (XmlRpcException e) {
      e.printStackTrace();
    }
  }
}