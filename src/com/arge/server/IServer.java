package com.arge.server;

import java.io.IOException;

public interface IServer {
  void start() throws IOException;
  void addHandler(String key, Class clazz);
}
