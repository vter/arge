package com.arge.client;

import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcCommonsTransportFactory;

import java.net.URL;

public class UpdateClient {
	
	public static void main(String[] args) throws Exception {		
		
		if (args.length < 3) {
			System.out.println("Usage:\nUpdateClient <client-address> <client-port> <value>");
      System.exit(0);
		}

    String clientAddress = args[0];
    int clientPort = Integer.parseInt(args[1]);
    double value = Double.parseDouble(args[2]);

    XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
    config.setServerURL(new URL("http://" + clientAddress + ":" + clientPort + "/xmlrpc"));
    config.setEnabledForExtensions(true);
    config.setConnectionTimeout(60 * 1000);
    config.setReplyTimeout(60 * 1000);

    XmlRpcClient client = new XmlRpcClient();

    client.setTransportFactory(new XmlRpcCommonsTransportFactory(client));
    client.setConfig(config);

    Object[] params = new Object[] { value };
    client.execute("Client.updateDelta", params);
	}

}
