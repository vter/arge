package com.arge.client;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcCommonsTransportFactory;

import java.net.MalformedURLException;
import java.net.URL;

public class ClientRequest extends Thread {

  private XmlRpcClient xmlRpcClient;

  public ClientRequest(String remoteAddress, int remotePort) {
    XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
    try {
      config.setServerURL(new URL("http://" + remoteAddress + ":" + remotePort + "/xmlrpc"));
      config.setEnabledForExtensions(true);
      config.setConnectionTimeout(60 * 1000);
      config.setReplyTimeout(60 * 1000);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }

    xmlRpcClient = new XmlRpcClient();

    xmlRpcClient.setTransportFactory(new XmlRpcCommonsTransportFactory(xmlRpcClient));
    xmlRpcClient.setConfig(config);
  }

  @Override
  public void run() {
    int n = 10;
    Object[] params = new Object[] {n};
    Integer result;
    try {
      result = (Integer) xmlRpcClient.execute("Balancer.work", params);
      System.out.println("Nombre de diviseurs de " + (Math.pow(2, n) - 1) + " : " + result);
    } catch (XmlRpcException e) {
      e.printStackTrace();
    }
  }
}
