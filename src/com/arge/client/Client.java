package com.arge.client;

import com.arge.server.IServer;
import com.arge.server.Server;

import java.io.IOException;

public class Client implements IClient {

  // Server representing the client
  private IServer server;

  // Time between requests
  private static double delta = 5000d;

  public Client() {}

  public Client(int port) {
    server = new Server(port);
  }

  @Override
  public void start() throws IOException {
    server.start();
  }

  @Override
  public void addHandler(String key, Class clazz) {
    server.addHandler(key, clazz);
  }

  @Override
  public int updateDelta(double x) {
    System.out.println("Changement du nombre de requêtes par secondes : " + x);
    delta = 1000.0 / x;
    return 0;
  }

  @Override
  public double getDelta() {
    return delta;
  }

  public static void main(String[] args) throws Exception {

    if (args.length < 3) {
      System.out.println("Usage:\nClient <local-port> <remote-host> <remote-port>");
      System.exit(0);
    }

    int clientPort = Integer.parseInt(args[0]);
    String remoteAddress = args[1];
    int remotePort = Integer.parseInt(args[2]);

    IClient client = new Client(clientPort);
    client.addHandler("Client", Client.class);
    client.start();
    System.out.println("Contacts " + remoteAddress + " on port " + remotePort);

    ClientRequest req;
    while (true) {
      req = new ClientRequest(remoteAddress, remotePort);
      req.setDaemon(true);
      req.start();
      Thread.sleep((long) client.getDelta());
    }
  }
}