package com.arge.client;

import com.arge.server.IServer;

public interface IClient extends IServer {
  int updateDelta(double x);
  double getDelta();
}
