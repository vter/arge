package com.arge.balancer;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcCommonsTransportFactory;

import java.net.MalformedURLException;
import java.net.URL;

public class BalancerRequest {

  private XmlRpcClient xmlRpcClient;
  private int number;

  public BalancerRequest(String remoteAddress, int number) {
    this.number = number;
    XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
    try {
      config.setServerURL(new URL(remoteAddress));
      config.setEnabledForExtensions(true);
      config.setConnectionTimeout(60 * 1000);
      config.setReplyTimeout(60 * 1000);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }

    xmlRpcClient = new XmlRpcClient();

    xmlRpcClient.setTransportFactory(new XmlRpcCommonsTransportFactory(xmlRpcClient));
    xmlRpcClient.setConfig(config);
  }

  public int send() {
    Object[] params = new Object[]{number};
    Integer result = -1;
    try {
      result = (Integer) xmlRpcClient.execute("Calculator.work", params);
    } catch (XmlRpcException e) {
      e.printStackTrace();
    }
    return result;
  }
}
