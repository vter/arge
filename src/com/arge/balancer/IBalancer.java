package com.arge.balancer;

import com.arge.server.IServer;

public interface IBalancer extends IServer {
  int addWorker(String address, int port);
  int delWorker(String address, int port);
  int work(int number);
}
