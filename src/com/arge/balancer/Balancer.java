package com.arge.balancer;

import com.arge.server.IServer;
import com.arge.server.Server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Balancer implements IBalancer {

  private IServer server;
  private static List<String> remoteList = new ArrayList<String>();
  private static  int nextWorkerIndex = -1;

  // This constructor is needed because each call to the Balancer through http instantiates a new Balancer
  public Balancer() {}

  public Balancer(int port) {
    System.out.println("Param balancer constructor");
    server = new Server(port);
  }

  @Override
  public void start() throws IOException {
    server.start();
  }

  @Override
  public void addHandler(String key, Class clazz) {
    server.addHandler(key, clazz);
  }

  @Override
  public int addWorker(String address, int port) {
    System.out.println("Adding [http://" + address + ":" + port + "] as a remote worker");
    remoteList.add("http://" + address + ":" + port);
    ++nextWorkerIndex;
    return 0;
  }

  @Override
  public int delWorker(String address, int port) {
    System.out.println("Removing worker [http://" + address + ":" + port + "]");
    remoteList.remove("http://" + address + ":" + port);
    // TODO tmp
    nextWorkerIndex = remoteList.isEmpty() ? -1 : 0;
    return 0;
  }

  @Override
  public int work(int number) {
    if (nextWorkerIndex == -1) {
      return -1;
    }

    int tmp = nextWorkerIndex;
    nextWorkerIndex = (nextWorkerIndex + 1) % remoteList.size();
    System.out.println("Envoie de la prochaine requête vers : " + remoteList.get(tmp));

    BalancerRequest request = new BalancerRequest(remoteList.get(nextWorkerIndex), number);
    return request.send();
  }

  public static void main(String[] args) throws Exception {

    if (args.length < 1) {
      System.out.println("Usage:\nBalancer <port>");
      System.exit(0);
    }

    int port = Integer.parseInt(args[0]);

    IBalancer balancer = new Balancer(port);
    balancer.addHandler("Balancer", Balancer.class);
    balancer.start();
  }
}
