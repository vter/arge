package com.arge.balancer;

import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcCommonsTransportFactory;

import java.net.URL;

public class UpdateBalancer {

  public static void main(String[] args) throws Exception {

    if (args.length < 5) {
      System.out.println("Usage:\nUpdateBalancer <balancer-address> <balancer-port> ADD|DEL <worker-address> <worker-port>");
      System.exit(0);
    }

    String balancerAddress = args[0];
    int balancerPort = Integer.parseInt(args[1]);
    String workerAddress = args[3];
    int workerPort = Integer.parseInt(args[4]);

    System.out.println("http://" + balancerAddress + ":" + balancerPort + "/xmlrpc");

    XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
    config.setServerURL(new URL("http://" + balancerAddress + ":" + balancerPort + "/xmlrpc"));
    config.setEnabledForExtensions(true);
    config.setConnectionTimeout(60 * 1000);
    config.setReplyTimeout(60 * 1000);

    XmlRpcClient proxy = new XmlRpcClient();

    proxy.setTransportFactory(new XmlRpcCommonsTransportFactory(proxy));
    proxy.setConfig(config);

    Object[] params = new Object[] { workerAddress, workerPort };
    if (args[2].equals("ADD")) {
      proxy.execute("Balancer.addWorker", params);
    } else if (args[2].equals("DEL")) {
      proxy.execute("Balancer.delWorker", params);
    }
  }
}
