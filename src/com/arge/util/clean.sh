#!/bin/sh
rm -rf arge
rm -f arge.zip
onevm list --xml | grep -E '<ID>[0-9]*</ID>' | grep -o '[0-9]*' | while read -r id
do
    onevm delete $id
    if [ $? -eq 0 ]; then
        echo "VM supprimee. ID:" $id
    fi
done