package com.arge.util;

import com.jcraft.jsch.*;

public class SshConnection {

  private String login;
  private String targetIp;

  private Session session;
  private Channel channel;

  public SshConnection(String login, String ip, String password) {
    this.login = login;
    targetIp = ip;
    JSch jsch = new JSch();

    try {
      session = jsch.getSession(login, ip, 22);
      session.setPassword(password);
      session.setConfig("StrictHostKeyChecking", "no");
    } catch (JSchException e) {
      e.printStackTrace();
    }
  }

  public void executeCommand(String command) throws JSchException, InterruptedException {
    System.out.println("Connection to: " + login + "@" + targetIp);
    session.connect();
    channel = session.openChannel("exec");
    channel.setInputStream(null);
    channel.setOutputStream(System.out);

    ((ChannelExec) channel).setCommand(command);
    ((ChannelExec) channel).setErrStream(System.out);
    channel.connect();
    Thread.sleep(12000);
    disconnect();
  }

  public void disconnect() {
    channel.disconnect();
    //session.disconnect();
    System.out.println("Closed connection to " + targetIp);
  }
}
