package com.arge.command;

import com.jcraft.jsch.JSchException;
import org.apache.xmlrpc.XmlRpcException;

public class DeleteVMCommand extends AbstractNebulaCommand {

  private final static String COMMAND = "one.vm.action";

  private boolean success;
  private int vmId;
  private int code;

  @Override
  public void execute(Object[] params) throws InterruptedException, JSchException {
    int id = Integer.parseInt(params[2].toString());
    Object[] val = null;
    try {
      System.out.println("Deleting VM " + id + " ...");
      val = (Object[]) xmlRpcClient.execute(COMMAND, params);
    } catch (XmlRpcException e) {
      e.printStackTrace();
    }

    assert val != null;
    assert val.length == 3;

    try {
      success = Boolean.parseBoolean(val[0].toString());
      assert success;

      vmId = Integer.parseInt(val[1].toString());
      code = Integer.parseInt(val[2].toString());
    } catch (NumberFormatException e) {
      throw new IllegalStateException("DeleteVMCommand:\n" + val[1].toString());
    }
  }

  public boolean isSuccess() {
    return success;
  }

  public int getVmId() {
    return vmId;
  }

  public int getCode() {
    return code;
  }
}
