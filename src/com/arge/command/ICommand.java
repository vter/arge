package com.arge.command;

import com.jcraft.jsch.JSchException;

public interface ICommand {
  void execute(Object[] params) throws InterruptedException, JSchException;
}
