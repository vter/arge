package com.arge.command;

public enum State {
  RUNNING(3);

  private int code;

  private State(int code) {
    this.code = code;
  }

  public int getCode() {
    return code;
  }
}
