package com.arge.command;

import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcCommonsTransportFactory;

import java.net.MalformedURLException;
import java.net.URL;

public abstract class AbstractNebulaCommand implements ICommand {

  protected final static String CLOUDMIP_URL = "http://cloudmip.univ-tlse3.fr:2633/RPC2";

  protected XmlRpcClient xmlRpcClient;

  public AbstractNebulaCommand() {
    XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
    try {
      config.setServerURL(new URL(CLOUDMIP_URL));
      config.setEnabledForExtensions(true);
      config.setConnectionTimeout(60 * 1000);
      config.setReplyTimeout(60 * 1000);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }

    xmlRpcClient = new XmlRpcClient();

    xmlRpcClient.setTransportFactory(new XmlRpcCommonsTransportFactory(xmlRpcClient));
    xmlRpcClient.setConfig(config);
  }
}
