package com.arge.command;

import org.apache.xmlrpc.XmlRpcException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

public class SearchVMStateCommand extends AbstractNebulaCommand {
  private final static String COMMAND = "one.vm.info";

  private boolean success;
  private String result;
  private int code;

  private int state;

  @Override
  public void execute(Object[] params) {
    Object[] val = null;
    try {
      System.out.println("Searching VM current state...");
      val = (Object[]) xmlRpcClient.execute(COMMAND, params);
    } catch (XmlRpcException e) {
      e.printStackTrace();
    }

    assert val != null;
    assert val.length == 3;

    success = Boolean.parseBoolean(val[0].toString());
    assert success;

    result = val[1].toString();
    code = Integer.parseInt(val[2].toString());
    System.out.println(result);
    state = parseStateFromString(result);
  }

  private int parseStateFromString(String xmlString) {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder;
    Document doc = null;
    try {
      builder = factory.newDocumentBuilder();
      InputSource is = new InputSource(new StringReader(xmlString));
      doc = builder.parse(is);
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    assert doc != null;

    NodeList nodeList = doc.getElementsByTagName("LCM_STATE");
    Node node = nodeList.item(0);
    return Integer.parseInt(node.getTextContent());
  }

  public boolean isSuccess() {
    return success;
  }

  public String getResult() {
    return result;
  }

  public int getCode() {
    return code;
  }

  public int getState() {
    return state;
  }
}
