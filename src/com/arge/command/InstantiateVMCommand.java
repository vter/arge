package com.arge.command;

import org.apache.xmlrpc.XmlRpcException;

public class InstantiateVMCommand extends AbstractNebulaCommand {

  private final static String COMMAND = "one.template.instantiate";

  private boolean success;
  private int vmId;
  private int code;

  @Override
  public void execute(Object[] params) {
    Object[] val = null;
    try {
      System.out.println("Instantiating VM...");
      val = (Object[]) xmlRpcClient.execute(COMMAND, params);
    } catch (XmlRpcException e) {
      e.printStackTrace();
    }

    assert val != null;
    assert val.length == 3;

    try {
      success = Boolean.parseBoolean(val[0].toString());
      assert success;

      vmId = Integer.parseInt(val[1].toString());
      code = Integer.parseInt(val[2].toString());
    } catch (NumberFormatException e) {
      throw new IllegalStateException("InstantiateVMCommand:\n" + val[1].toString());
    }
  }

  public boolean isSuccess() {
    return success;
  }

  public int getVmId() {
    return vmId;
  }

  public int getCode() {
    return code;
  }
}
