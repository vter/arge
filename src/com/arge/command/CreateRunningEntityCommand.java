package com.arge.command;

import com.arge.launcher.Launcher;
import com.arge.util.SshConnection;
import com.jcraft.jsch.JSchException;

public class CreateRunningEntityCommand extends AbstractNebulaCommand {

  private int vmId;
  private String vmIp;

  @Override
  public void execute(Object[] params) throws InterruptedException, JSchException {
    String command = params[0].toString();
    InstantiateVMCommand instantiateVMCommand = new InstantiateVMCommand();
    instantiateVMCommand.execute(new Object[]{Launcher.SESSION, 2, ""});

    SearchVMIPCommand vmIpCommand = new SearchVMIPCommand();
    vmIpCommand.execute(new Object[]{Launcher.SESSION, instantiateVMCommand.getVmId()});

    SearchVMStateCommand vmStateCommand = new SearchVMStateCommand();
    Object[] vmStateCommandParams = new Object[]{Launcher.SESSION, instantiateVMCommand.getVmId()};
    vmStateCommand.execute(vmStateCommandParams);

    vmId = instantiateVMCommand.getVmId();
    vmIp = vmIpCommand.getIp();

    while (vmStateCommand.getState() != 3) {
      Thread.sleep(5000);
      vmStateCommand.execute(vmStateCommandParams);
    }

    // Apparently required
    Thread.sleep(10000);

    System.out.println("VM " + vmId + " (" + vmIp + ") now running.");

    SshConnection connection = new SshConnection(Launcher.LOGIN, vmIpCommand.getIp(), Launcher.PASSWORD);
    System.out.println(command);
    connection.executeCommand(command);
  }

  public int getVmId() {
    return vmId;
  }

  public String getVmIp() {
    return vmIp;
  }
}
