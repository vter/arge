package com.arge.launcher;

import com.arge.server.IServer;
import com.jcraft.jsch.JSchException;

public interface ILauncher extends IServer {
  int createWorker(int port) throws JSchException, InterruptedException;
  int deleteWorker(String address) throws JSchException, InterruptedException;
  int listWorkers();
}
