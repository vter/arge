package com.arge.launcher;

import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcCommonsTransportFactory;

import java.net.URL;

public class UpdateLauncher {

  public static void main(String[] args) throws Exception {

    if (args.length < 4) {
      System.out.println("Usage:\nUpdateLauncher <launcher-address> <launcher-port> ADD|DEL|LIST <worker-port>");
      System.exit(0);
    }

    String launcherAddress = args[0];
    int launcherPort = Integer.parseInt(args[1]);
    int workerPort = Integer.parseInt(args[3]);

    System.out.println("http://" + launcherAddress + ":" + launcherPort + "/xmlrpc");

    XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
    config.setServerURL(new URL("http://" + launcherAddress + ":" + launcherPort + "/xmlrpc"));
    config.setEnabledForExtensions(true);
    config.setConnectionTimeout(60 * 1000);
    config.setReplyTimeout(60 * 1000);

    XmlRpcClient proxy = new XmlRpcClient();

    proxy.setTransportFactory(new XmlRpcCommonsTransportFactory(proxy));
    proxy.setConfig(config);

    if (args[2].equals("LIST")) {
      proxy.execute("Launcher.listWorkers", new Object[] {});
    }

    else {
      Object[] params = new Object[] { workerPort };
      if (args[2].equals("ADD")) {
        proxy.execute("Launcher.createWorker", params);
      } else if (args[2].equals("DEL")) {
        proxy.execute("Launcher.deleteWorker", params);
      }
    }
  }
}
