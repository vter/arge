#!/bin/sh
ROOT_DIR=/cloudmip/home/ens36/arge
LOG_FILE=/cloudmip/home/ens36/run.log

rm -f ${LOG_FILE}
touch ${LOG_FILE}

case $1 in
  "launcher")
    if [ -n $2 ] ; then
      echo "Starting $1 on port $2"
      java -XX:MaxHeapSize=256m -jar ${ROOT_DIR}/$1.jar $2
    else
      echo "Usage: $0 $1 <port>"
    fi
    ;;
  "worker")
    if [ -n $2 ] ; then
      echo "Starting $1 on port $2"
      java -jar ${ROOT_DIR}/$1.jar $2 &
    else
      echo "Usage: $0 $1 <port>"
    fi
    ;;
  "balancer")
    if [ -n $2 ] ; then
      echo "Starting $1 on port $2"
      java -jar ${ROOT_DIR}/$1.jar $2
    else
      echo "Usage: $0 $1 <port>"
    fi
    ;;
  "client")
    if [ -n $2 ] && [ -n $3 ] && [ -n $4 ] ; then
      echo "Starting $1 on port $2 with remote balancer on [$3:$4]"
      java -jar ${ROOT_DIR}/$1.jar $2 $3 $4
    else
      echo "Usage: $0 $1 <port> <balancer-ip> <balancer-port>"
    fi
    ;;
  "ubal")
    if [ -n $2 ] && [ -n $3 ] && [ -n $4 ] && [ -n $5 ] && [ -n $6 ] ; then
      echo "Updating balancer [$2:$3], $4 worker [$5:$6]"
      java -jar ${ROOT_DIR}/$1.jar $2 $3 $4 $5 $6
    else
      echo "Usage: $0 $1 <balancer-ip> <balancer-port> ADD|DEL <worker-ip> <worker-port>"
    fi
    ;;
  "ucli")
    if [ -n $2 ] && [ -n $3 ] && [ -n $4 ] ; then
      echo "Updating client [$2:$3] with value $4"
      java -jar ${ROOT_DIR}/$1.jar $2 $3 $4
    else
      echo "Usage: $0 $1 <port> <balancer-ip> <balancer-port>"
    fi
    ;;
  *)
    echo "Usage: $0 <launcher|worker|balancer|client|ubal|ucli> <params>"
    ;;
esac
