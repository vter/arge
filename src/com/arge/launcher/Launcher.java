package com.arge.launcher;

import com.arge.command.*;
import com.arge.server.IServer;
import com.arge.server.Server;
import com.arge.util.SshConnection;
import com.jcraft.jsch.JSchException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Launcher implements ILauncher {

  public final static String LOGIN = "ens36";
  public final static String PASSWORD = "T7VLBE";
  public final static String SESSION = LOGIN + ":" + PASSWORD;
  public final static String ROOT_DIR = "/cloudmip/home/ens36/arge";

  private static Map<String, Integer> runningVms = new HashMap<String, Integer>();

  IServer server;

  public Launcher() {}

  public Launcher(int port) {
    server = new Server(port);
  }

  @Override
  public int createWorker(int port) throws JSchException, InterruptedException {
    String command = "cd " + ROOT_DIR + " && ./runner.sh worker " + port;
    CreateRunningEntityCommand cmd = new CreateRunningEntityCommand();
    cmd.execute(new Object[] { command });
    runningVms.put(cmd.getVmIp(), cmd.getVmId());
    return 0;
  }

  @Override
  public int deleteWorker(String address) throws JSchException, InterruptedException {
    int vmId = runningVms.get(address);
    DeleteVMCommand deleteVMCommand = new DeleteVMCommand();
    deleteVMCommand.execute(new Object[] { SESSION, "finalize", vmId });
    runningVms.remove(address);
    return 0;
  }

  @Override
  public int listWorkers() {
    System.out.println("==================== RUNNING VMs ====================");
    for (Map.Entry<String, Integer> entry : runningVms.entrySet()) {
      System.out.println("VM " + entry.getValue() + ": " + entry.getKey());
    }
    System.out.println("=====================================================");
    return 0;
  }

  @Override
  public void start() throws IOException {
    server.start();
  }

  @Override
  public void addHandler(String key, Class clazz) {
    server.addHandler(key, clazz);
  }

  public static void main(String[] args) throws Exception {

    if (args.length < 1) {
      System.out.println("Usage:\nLauncher <port>");
      System.exit(0);
    }

    int port = Integer.parseInt(args[0]);

    // Lance un worker sur une VM
    String command = "cd " + ROOT_DIR + " && ./runner.sh worker 9000";
    CreateRunningEntityCommand cmd = new CreateRunningEntityCommand();
    cmd.execute(new Object[]{ command });


    // Lance un répartiteur et un client sur une autre VM
    String command2 = "cd " + ROOT_DIR +
        " && ./runner.sh balancer 8000 &" +
        " && ./runner.sh ubal localhost 8000 ADD " + cmd.getVmIp() + " 9000 &" +
        " && ./runner.sh client 7000 localhost 8000 &";
    CreateRunningEntityCommand cmd2 = new CreateRunningEntityCommand();
    cmd2.execute(new Object[]{ command });

    ILauncher launcher = new Launcher(port);
    launcher.addHandler("Launcher", Launcher.class);
    launcher.start();
  }
}
