package com.arge.worker;

public class Calculator {

  public int work(int i) {
    System.out.println("Calcul du nombre de diviseurs de " + i);
    double d = Math.pow(2, i) - 1;

    int res = 0;
    for (int j = 1; j <= d; ++j) {
      if (d % j == 0) {
        ++res;
      }
    }

    return res;
  }
}
