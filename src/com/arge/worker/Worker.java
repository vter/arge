package com.arge.worker;

import com.arge.server.IServer;
import com.arge.server.Server;

import java.io.IOException;

public class Worker implements IWorker {

  private IServer server;

  public Worker(int port) {
    server = new Server(port);
  }

  @Override
  public void start() throws IOException {
    server.start();
  }

  @Override
  public void addHandler(String key, Class clazz) {
    server.addHandler(key, clazz);
  }

  public static void main(String[] args) throws Exception {

    if (args.length < 1) {
      System.out.println("Usage:\n Worker <port>");
      System.exit(0);
    }

    /*File file = new File(Launcher.ROOT_DIR + "/run.log");
    FileOutputStream fos = new FileOutputStream(file);
    PrintStream ps = new PrintStream(fos);
    System.setOut(ps);*/

    int port = Integer.parseInt(args[0]);

    IWorker worker = new Worker(port);
    worker.addHandler("Calculator", Calculator.class);
    worker.start();
  }
}
